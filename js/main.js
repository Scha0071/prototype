function checkInput(){
  var search = document.getElementById('search').value;
  var searchLength = search.length;
  if (searchLength == 10 || searchLength == 13){
      document.getElementById('results').value = ""
//  console.log(search)

      $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=isbn:" + search,
        dataType: "json",

        success: function(data) {
          if($(data.items).length > 0){
            $('#results').val(data.items[0].volumeInfo.title);
            $('#results').attr('readonly','true');
            $('#search').css({"border-color": "inherit", "box-shadow": "inherit"});
            document.getElementById('comment').innerHTML="";
          } else {
            $('#search').css({"border-color": "red", "box-shadow": "0px 0px 5px #ff0000"});
            $('#results').removeAttr('readonly');
            document.getElementById('comment').innerHTML="Let op! Dit ISBN wordt niet gevonden in de Google Books API.";
          }

        },

        type: 'GET'
      });
    }

}

document.getElementById('search').addEventListener('keyup', checkInput, true)
